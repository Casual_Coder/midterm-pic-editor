#ifndef GRAYSCALE_H
#define GRAYSCALE_H

#include <stdio.h>
#include "structHeader.h"

// return 0 if failed, 1 if successful
void apply_grayscale(image *picture);

#endif
