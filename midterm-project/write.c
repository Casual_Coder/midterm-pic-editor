#include <stddef.h>
#include <stdio.h>
#include "structHeader.h"

void write(image pic, char pic_name[]){
  FILE *fp = fopen(pic_name, "wb");

  char filler_line[255] = "# CREATOR: GIMP PNM Filter Version 1.1";
  fprintf(fp, "P6\n%s\n%d %d\n%d\n", filler_line, pic.cols, pic.rows, pic.colors);
  
  fwrite(pic.data, sizeof(pixel), pic.cols*pic.rows, fp);
  fclose(fp);
}
