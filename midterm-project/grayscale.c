#include <stdio.h>
#include "structHeader.h"

void apply_grayscale(image *picture) {
  for (int i = 0; i < picture->rows * picture->cols; i++) {
    unsigned int intensity = 0.3 * picture->data[i].r + 0.59 * picture->data[i].g + 0.11 * picture->data[i].b;
    picture->data[i].r = intensity;
    picture->data[i].g = intensity;
    picture->data[i].b = intensity;
  }
}
