#include <stdio.h>
#include <string.h>
#include "brightness.h"
#include "read.h"
#include "write.h"
#include "grayscale.h"
#include "crop.h"
#include "switchColors.h"
#include "contrast.h"
#include "sharpen.h"
#include "structHeader.h"
#include "edgeDetector.h"

void print_menu() {
  printf("r <filename> - read image from <filename>\n");
  printf("w <filename> - write image to <filename>\n");
  printf("s - swap color channels\n");
  printf("br <amt> - change brightness (up or down) by the given amount\n");
  printf("c <x1> <y1> <x2> <y2> - crop image to the box with the given corners\n");
  printf("g - convert to grayscale\n");
  printf("cn <amt> - change contrast (up or down) by the given amount\n");
  printf("bl <sigma> - Gaussian blur with the given radius (sigma)\n");
  printf("sh <sigma> <amt> - sharpen by given amount (intensity), with radius (sigma)\n");
  printf("e <sigma> <threshold> - detect edges with intensity gradient above given threshold\n");
  printf("q - quit\n");
}

int handle_options() {
  int continue_running = 1;
  int has_image = 0;
  image picture;
  while(continue_running) {
    print_menu();
    char response[255];
    char option[3];
    char optional_param1[255];
    char optional_param2[255];
    char optional_param3[255];
    char optional_param4[255];
    fgets(response, 255, stdin);
    int num_arguments = sscanf(response, "%s %s %s %s %s\n", option, optional_param1, optional_param2, optional_param3, optional_param4);
    if (num_arguments == 1) {
      if (strcmp(option, "s") == 0) {
	// Create a pointer to feed into switchColors function
	if (has_image != 0) {
	  image *pic_pointer = &picture;
	  switchColors(pic_pointer);
	} else {
	  printf("No image has been loaded\n");
	}
      } else if (strcmp(option, "g") == 0) {
	// Feeds in information to change saved picture
	if (has_image != 0) {
	  image *pic_pointer = &picture;
	  apply_grayscale(pic_pointer);
	} else {
	  printf("No image has been loaded\n");
	}
      } else if (strcmp(option, "q") == 0) {
	// quits the program
	if (has_image)
	  free(picture.data);
	return 0;
      } else {
	printf("Option not valid\n");
      }
    }

    else if (num_arguments == 2) {
      if (strcmp(option, "br") == 0) {
	// adjust brightness
	if (has_image != 0) {
	  int param = atoi(optional_param1);
	  image *pic_pointer = &picture;
	  adjust_brightness(param, pic_pointer);
	} else {
	  printf("No image has been loaded\n");
	}
      } else if (strcmp(option, "r") == 0){
	// read function
	if (has_image) {
	  free(picture.data);
	}
	picture = read(optional_param1);
	has_image = 1;
      } else if (strcmp(option, "w") == 0) {
	// write function
	if (has_image != 0) {
	  write(picture, optional_param1);
	} else {
	  printf("No image has been loaded\n");
	}
      } else if (strcmp(option, "cn") == 0) {
	if (has_image != 0) {
	  double param = atof(optional_param1);
	  image *pic_pointer = &picture;
	  // apply contrast
	  apply_contrast(pic_pointer, param);
	} else {
	  printf("No image has been loaded\n");
	}
      } else if (strcmp(option, "bl") == 0) {
	// blur function
	if (has_image != 0) {
	  double param = atof(optional_param1);
	  image *pointer = &picture;
	  blur(pointer, param);
	} else {
	  printf("No image has been loaded\n");
	}
      } else {
	printf("Option not valid\n");
      }
    }

    else if (num_arguments == 3) {
      if (strcmp(option, "sh") == 0) {
	if (has_image != 0) {
	  double sigma = atof(optional_param1);
	  double amt = atof(optional_param2);
	  image *pointer = &picture;
	  // Creates pointer to feed into sharpen function
	  sharpen(pointer, sigma, amt, picture);
	} else {
	  printf("No image has been loaded\n");
	}
      } else if (strcmp(option, "e") == 0) {
	if (has_image != 0){
	  double param1 = atof(optional_param1);
	  int param2 = atoi(optional_param2);
	  image *picture_pointer = &picture;
	  edgeDetector(picture_pointer, param1, param2);
	} else {
	  printf("No image has been loaded\n"); 
	}
      } else {
	// Anything other than sh param1 param2 param3 is not valid
	printf("Option not valid\n");
      }
    }

    else if (num_arguments == 4)
      printf("Option not valid\n");
    
    else if (num_arguments == 5) {
      if (strcmp(option, "c") == 0) {
	if (has_image != 0) {
	  int param1 = atoi(optional_param1);
	  int param2 = atoi(optional_param2);
	  int param3 = atoi(optional_param3);
	  int param4 = atoi(optional_param4);
	  // Creates pointer to feed into crop function
	  image *picture_pointer = &picture;
	  printf("eh");
	  crop(picture_pointer, param1, param2, param3, param4);
	} else {
	  printf("No image has been loaded\n");	
	}
      } else {
	printf("Option not valid\n");
      }
    }
    else
      printf("Option not valid\n");
  }
  free(picture.data);
  return 1;
}
