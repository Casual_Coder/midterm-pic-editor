#ifndef STRUCT_HEADER_H
#define STRUCT_HEADER_H

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

typedef struct {
  unsigned char r;
  unsigned char g;
  unsigned char b;
} pixel;

typedef struct {
  pixel *data;
  int rows;
  int cols;
  int colors;
} image;
#endif
