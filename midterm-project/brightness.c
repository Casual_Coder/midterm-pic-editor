#include <stdio.h>
#include "structHeader.h"

void adjust_brightness(int amount, image *picture) {
  
  for (int i = 0; i < picture->rows * picture->cols; i++) {
    if (picture->data[i].r < (255 - amount) && picture->data[i].r > -amount) {
      picture->data[i].r += amount;
    } else if (picture->data[i].r > (255 - amount)) {
      picture->data[i].r = 255;
    } else {
      picture->data[i].r = 0;
    }
    
    if (picture->data[i].g < (255 - amount) && picture->data[i].g > -amount) {
      picture->data[i].g += amount;
    } else if (picture->data[i].g > (255 - amount)) {
      picture->data[i].g = 255;
    } else {
      picture->data[i].g = 0;
    }

    if (picture->data[i].b < (255 - amount) && picture->data[i].b > -amount) {
      picture->data[i].b += amount;
    } else if (picture->data[i].b > (255 - amount)) {
      picture->data[i].b = 255;
    } else {
      picture->data[i].b = 0;
    }
  }
}
