#include <stddef.h>
#include <stdio.h>
#include "structHeader.h"


image read(char* pic_name) {
  int col, row, color;
  char whocares[255];
  FILE *fp = fopen(pic_name, "rb");
  fgets(whocares, 255, fp);
  fgets(whocares, 255, fp);
  fscanf(fp, "%d %d\n%d\n", &col, &row, &color);
  
  pixel *pix0 = malloc(sizeof(pixel)*row*col);
  fread(pix0, sizeof(pixel), row*col, fp);

  image pic = {pix0, row, col, color};
  fclose(fp);
  return pic;
}

