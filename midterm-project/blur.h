#ifndef BLUR_H
#define BLUR_H
#include "structHeader.h"

void blur(image *pic, double sigma);

double *make_gauss(double sigma, int dim);

double *apply_gauss(image *pic, int row, int col, double *gauss, int dim, int sum);



#endif
