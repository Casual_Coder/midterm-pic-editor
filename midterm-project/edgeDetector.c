#include <math.h>
#include <stdio.h>
#include "structHeader.h"
#include "blur.h"
#include "grayscale.h"

void edgeDetector(image *pic, double sigma, int thresh){

  apply_grayscale(pic);
  printf("%d\n", pic->data[0].r);

  blur(pic, sigma);
  printf("%d\n", pic->data[0].r);


  pixel *edges = malloc(pic->rows*pic->cols*sizeof(pixel));
  
  double i_x, i_y, i_grad;
  for(int row = 1; row < pic->rows - 1; row++){
    for(int col = 1; col < pic->cols - 1; col++){

      i_x = (double)(pic->data[row*pic->cols + col + 1].r - pic->data[row*pic->cols + col - 1].r) / 2.0;
      i_y = (double)(pic->data[(row + 1)*pic->cols + col].r - pic->data[(row - 1)*pic->cols + col].r) / 2.0;
      i_grad = sqrt(pow(i_x, 2) + pow(i_y, 2));

      //      printf("%d,%d  %d,%d\n", pic->data[row*pic->cols + col + 1].r, pic->data[row*pic->cols + col - 1].r, pic->data[(row + 1)*pic->cols + col].r, pic->data[(row - 1)*pic->cols + col].r);
      if(i_grad >= thresh){
	edges[row*pic->cols + col].r = 0;
	edges[row*pic->cols + col].g = 0;
	edges[row*pic->cols + col].b = 0;
      }
      else{
	edges[row*pic->cols + col].r = 255;
	edges[row*pic->cols + col].g = 255;
	edges[row*pic->cols + col].b = 255;
      }
    }
  }

  free(pic->data);
  pic->data = edges;
}
