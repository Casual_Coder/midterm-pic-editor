#ifndef EDGEDETECTOR_H
#define EDGEDETECTOR_H
#include "structHeader.h"
#include "blur.h"
#include "grayscale.h"

//double x_grad(image *pic, int x, int y);

//double y_grad(image *pic, int x, int y);

void edgeDetector(image *pic, double sigma, int thresh);

#endif
