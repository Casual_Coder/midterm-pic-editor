#include <stdio.h>
#include "structHeader.h"

void apply_contrast(image *pic, double adjustment) {
  for (int i = 0; i < pic->rows * pic->cols; i++) {
    double red = ((double)pic->data[i].r/255) - 0.5;
    double blue = ((double)pic->data[i].b/255) - 0.5;
    double green = ((double)pic->data[i].g/255) - 0.5;
    if (red * adjustment < 0.5 && red * adjustment > -0.5) {
      red *= adjustment;
    } else if (red * adjustment > 0.5) {
      red = 0.5;
    } else {
      red = -0.5;
    }
    
    if (blue * adjustment < 0.5 && blue * adjustment > -0.5) {
      blue *= adjustment;
    } else if (blue * adjustment > 0.5) {
      blue = 0.5;
    } else {
      blue = -0.5;
    }
     
    if (green * adjustment < 0.5 && green * adjustment > -0.5) {
       green *= adjustment;
    } else if (green * adjustment > 0.5) {
      green = 0.5;
    } else {
      green = -0.5;
    }

    pic->data[i].r = (red + 0.5)*255;
    pic->data[i].g = (green + 0.5)*255;
    pic->data[i].b = (blue + 0.5)*255;
  }
}
