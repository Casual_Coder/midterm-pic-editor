#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include "structHeader.h"

void crop(image *pic, int ux, int uy, int lx, int ly){
  pixel *new_pic_data = malloc((ly - uy)*(lx - ux)*sizeof(pixel));

  if(new_pic_data == NULL)
    printf("There's a problem");

  for(int row = 0; row < ly - uy; row++){
    for(int col = 0; col < lx - ux; col++){
      new_pic_data[row*(lx-ux) + col] = pic->data[(uy + row)*(pic->cols) + (col + ux)];
    }
  }
  pic->rows = ly - uy;
  pic->cols = lx - ux;
  free(pic->data);
  pic->data = new_pic_data;
}
