#include <math.h>
#include "structHeader.h"
#define PI 3.141592653



double *make_gauss(double sigma, int dim) {
  double *gauss = malloc(dim*dim*sizeof(double));
  for(int c1 = 0; c1 < dim; c1++) {
    for(int c2 = 0; c2 < dim; c2++){
      int dy = c1 - dim/2;
      int dx = c2 - dim/2;
      gauss[c1*dim + c2] = (1.0 / (2.0 * PI * pow(sigma, 2)))
	* exp( -(pow(dx, 2) + pow(dy, 2)) / (2 * pow(sigma, 2)));
    }
  }
  return gauss;
}

double sum_matrix(double *matrix, int size) {
  double sum = 0;
  for (int i = 0; i < size; i++) {
    sum += matrix[i];
  }
  return sum;
}

double *apply_gauss(image *pic, int row, int col, double *gauss, int dim, double sum) {
  double g_r = 0;
  double g_g = 0;
  double g_b = 0;

  int bool = 1;
  while (bool) {
    int i = 0;
    for (int r1 = row - dim/2; r1 <= row + dim/2; r1++) {
      for (int c1 = col - dim/2; c1 <= col + dim/2; c1++) {
	if (r1 >= 0 && c1 >= 0 && r1 < pic->rows && c1 < pic->cols) {
	  g_r += gauss[i] * (double)(pic->data[r1*pic->cols + c1].r);
	  g_g += gauss[i] * (double)(pic->data[r1*pic->cols + c1].g);
	  g_b += gauss[i] * (double)(pic->data[r1*pic->cols + c1].b);
	  i++;
	} else {
	  i++;
	}
	if (i >= dim*dim) {
	  bool = 0;
	}
      }
    }
  }
  double *g_nums = malloc(3*sizeof(double));
  g_nums[0] = g_r / sum;
  g_nums[1] = g_g / sum;
  g_nums[2] = g_b / sum;
  return g_nums;
}




void blur(image *pic, double sigma) {
  int dim = 10 * sigma;
  if(dim%2 == 0) {
    dim++;
  }
  double *gauss;
  gauss = make_gauss(sigma, dim);
  double sum = sum_matrix(gauss, dim*dim);

  double *g_nums;
  for (int row = 0; row < pic->rows; row++) {
    for (int col = 0; col < pic->cols; col++) {
      g_nums = apply_gauss(pic, row, col, gauss, dim, sum);
      if (g_nums[0] > 255) {
	g_nums[0] = 255;
      }
      if (g_nums[1] > 255) {
	g_nums[1] = 255;
      }
      if (g_nums[2] > 255) {
	g_nums[2] = 255;
      }
      
      pic->data[row*pic->cols + col].r = (unsigned char)(g_nums[0]);
      pic->data[row*pic->cols + col].g = (unsigned char)(g_nums[1]);
      pic->data[row*pic->cols + col].b = (unsigned char)(g_nums[2]);
      free(g_nums);
    }
  }

  free(gauss);
}
