Melissa Shohet, Jonathan Wang
601.220
Section 2
Midterm Project #1
mshohet1@jhu.edu, jwang278@jhu.edu

I forget what's supposed to go in a README file, but let's just say the whole project wasn't a cakewalk. The one thing that I do know should go here is the fact that, if the tester calls write after calling edge detector, valgrind will complain. However, there are no memory leaks, and both Carolyn and Ariana said the error messages are fine and no points should be deducted for them.
