#ifndef BRIGHTNESS_H
#define BRIGHTNESS_H

#include <stdio.h>
#include "structHeader.h"

// return 0 if failed, 1 if successful
void adjust_brightness(int amount, image *picture);

#endif
