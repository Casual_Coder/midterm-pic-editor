#ifndef CONTRAST_H
#define CONTRAST_H

#include <stdio.h>
#include "structHeader.h"

// return 0 if failed, 1 if successful
image apply_contrast(image *pic, double adjustment);

#endif
