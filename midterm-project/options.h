#ifndef OPTIONS_H
#define OPTIONS_H

#include <stdio.h>

void print_menu();

int handle_options();

#endif
