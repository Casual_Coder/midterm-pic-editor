#include <math.h>
#include "structHeader.h"
#include "blur.h"
#define PI 3.141592653

void sharpen(image *pic, double sigma, double amt) {
  int dim = 10 * sigma;
  if(dim%2 == 0) {
    dim++;
  }
  double *gauss;
  gauss = make_gauss(sigma, dim);
  double sum = 0;
  for (int i = 0; i < dim*dim; i++) {
    sum += gauss[i]; 
  }

  double *g_nums;
  for (int row = 0; row < pic->rows; row++) {
    for (int col = 0; col < pic->cols; col++) {
      g_nums = apply_gauss(pic, row, col, gauss, dim, sum);
      if (g_nums[0] > 255) {
	g_nums[0] = 255;
      }
      if (g_nums[1] > 255) {
	g_nums[1] = 255;
      }
      if (g_nums[2] > 255) {
	g_nums[2] = 255;
      }

      int dif_r = ((pic->data[row*pic->cols + col].r)-(g_nums[0]));

      int dif_g = (pic->data[row*pic->cols + col].g)-(g_nums[1]);

      int dif_b = (pic->data[row*pic->cols + col].b)-(g_nums[2]);

      if (pic->data[row*pic->cols + col].r + (amt * dif_r) <= 255 && pic->data[row*pic->cols + col].r + (amt * dif_r) >= 0) {
	pic->data[row*pic->cols + col].r = (unsigned char)(pic->data[row*pic->cols + col].r + (amt*dif_r));

      } else if (pic->data[row*pic->cols + col].r + (amt * dif_r) > 255) {
	pic->data[row*pic->cols + col].r = 255;
      } else {
	pic->data[row*pic->cols + col].r = 0;
      }
      
      if (pic->data[row*pic->cols + col].g + (amt * dif_g) <= 255 && pic->data[row*pic->cols + col].g + (amt * dif_g) >= 0) {
	pic->data[row*pic->cols + col].g = (unsigned char)(pic->data[row*pic->cols + col].g + (amt*dif_g));

      } else  if (pic->data[row*pic->cols + col].g + (amt * dif_g) > 255) {
	pic->data[row*pic->cols + col].g = 255; 
      } else {
	pic->data[row*pic->cols + col].g = 0;
      }
	
      if (pic->data[row*pic->cols + col].b + (amt * dif_b) <= 255 && pic->data[row*pic->cols + col].b + (amt * dif_b) >= 0) {
	pic->data[row*pic->cols + col].b = (unsigned char)(pic->data[row*pic->cols + col].b + (amt*dif_b));

      } else if (pic->data[row*pic->cols + col].b + (amt * dif_b) > 255){
	pic->data[row*pic->cols + col].b = 255; 
      } else {
	pic->data[row*pic->cols + col].b = 0;
      }
      free(g_nums);
    }
  }
  free(gauss);
  
}
