#include <stddef.h>
#include <stdio.h>
#include "structHeader.h"

void switchColors(image *pic){
  char temp1;
  int place; //references the index in the matrix, because I don't want to retype it
  
  for(int row = 0; row < pic->rows; row++){
    for(int col = 0; col < pic->cols; col++){
      place = (int)(row*(pic->cols) + col);
      temp1 = pic->data[place].r;
      pic->data[place].r = pic->data[place].g;
      pic->data[place].g = pic->data[place].b;
      pic->data[place].b = temp1;
     
    }
  }

}
