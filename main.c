#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "read.h"
#include "write.h"

int main(){
  image pic = read();
  write(pic);
  free(pic.data);
  return 0;
}
